<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN”
“http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
   <head>
      <meta charset="UTF-8" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php
      echo "<title> Funciones y variables GET y POST en PHP  </title>";
    ?>
    </head>

    <body>
   
   <?php

$matricula1 ['ABC0129'] = array(
    'Auto'=> array(
    'marca' => 'Nissan',
    'modelo' => 2020,
    'tipo'=> 'camioneta',
    ),
    'Propietario'=> array(
    'nombre' => 'Sofia Cruz',
    'ciudad' => 'Chetumal, Quintana Roo',
    'direccion' => 'Calle Benito Juarez 122'
));

$matricula2 ['CDE1238'] = 
array(
    'Auto' => array(
    'marca' => 'Volkswagen',
    'modelo' => 2021,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Enrrique Gonzalez',
    'ciudad' => 'Puebla, Puebla',
    'direccion' => 'Avenida Flores Magón'
));

$matricula3 ['FGH3457'] = array(
        'Auto' => array(
        'marca' => 'Volkswagen',
        'modelo' => 2021,
        'tipo'=> 'sedan',
        ),
        'Propietario' => array(
        'nombre' => 'Carmen Ruiz',
        'ciudad' => 'Puebla, Puebla',
        'direccion' => 'Avenida Camelia'
    ));

$matricula4 ['IJK5670'] = array(
        'Auto' => array(
        'marca' => 'Honda',
        'modelo' => 2019,
        'tipo'=> 'hachback',
        ),
        'Propietario' => array(
        'nombre' => 'Carlos Rodriguez',
        'ciudad' => 'Zacatecas, Zacatecas',
        'direccion' => 'Calle Cuauhtemoc'
    ));
$matricula5 ['LMN9012'] = array(
    'Auto' => array(
    'marca' => 'Toyota',
    'modelo' => 2020,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Rosa Camacho',
    'ciudad' => 'Guadalajara, Jalisco',
    'direccion' => 'Calle Francisco I. Madero'
));
$matricula6 ['OPQ8123'] = array(
    'Auto' => array(
    'marca' => 'KIA',
    'modelo' => 2019,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Santiago López',
    'ciudad' => 'Hermosillo, Sonora ',
    'direccion' => 'Avenida de la Industria'
));
$matricula7 ['RST7345'] = array(
    'Auto' => array(
    'marca' => 'Volkswagen',
    'modelo' => 2021,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Santiago López',
    'ciudad' => 'Cuernavaca, Morelos',
    'direccion' => 'Avenida Fray Servando'
));
$matricula8 ['UVW0765'] = array(
    'Auto' => array(
    'marca' => 'Volkswagen',
    'modelo' => 2021,
    'tipo'=> 'sedan',
    ),
    'Propietario' => array(
    'nombre' => 'Sandra Remigio',
    'ciudad' => 'Mérida, Yucatán',
    'direccion' => 'Calle Niños Heroes'
));
$matricula9 ['ZAB0123'] = array(
    'Auto' => array(
    'marca' => 'Toyota',
    'modelo' => 2020,
    'tipo'=> 'hachback',
    ),
    'Propietario' => array(
    'nombre' => 'Pablo Gomez',
    'ciudad' => 'Puebla, Puebla',
    'direccion' => 'Avenida Nacional'
));
$matricula10 ['BCD4567'] = array(
    'Auto' => array(
    'marca' => 'Nissan',
    'modelo' => 2021,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Jake Hernandez',
    'ciudad' => 'Chihuahua, Chihuahua',
    'direccion' => 'Avenida Independencia'
));
$matricula11 ['DFG9013'] = array(
    'Auto' => array(
    'marca' => 'General Motors',
    'modelo' => 2021,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Noemi Aguilar',
    'ciudad' => 'Puebla, Puebla',
    'direccion' => 'Calle Porfitio Diaz'
));
$matricula12 ['HIJ3456'] = array(
    'Auto' => array(
    'marca' => 'SEAT',
    'modelo' => 2019,
    'tipo'=> 'hachback',
    ),
    'Propietario' => array(
    'nombre' => 'Gabriela Torres',
    'ciudad' => 'Morelia, Michoacán',
    'direccion' => 'Calle Las rosas'
));
$matricula13 ['KLM7890'] = array(
    'Auto' => array(
    'marca' => 'Toyota',
    'modelo' => 2021,
    'tipo'=> 'camioneta',
    ),
    'Propietario' => array(
    'nombre' => 'Marcos Alcantara',
    'ciudad' => 'Oaxaca de Juárez, Oaxaca',
    'direccion' => 'Calle Jardin Hidalgo'
));
$matricula14 ['AJK0928'] = array(
    'Auto' => array(
    'marca' => 'Nissan',
    'modelo' => 2021,
    'tipo'=> 'sedan',
    ),
    'Propietario' => array(
    'nombre' => 'Daniel Duran',
    'ciudad' => 'Tepic, Nayarit',
    'direccion' => 'Avenida 5 de mayo'
));
$matricula15 ['ABC0419'] = array(
    'Auto' => array(
    'marca' => 'KIA',
    'modelo' => 2020,
    'tipo'=> 'sedan',
    ),
    'Propietario' => array(
    'nombre' => 'Angel Moralez',
    'ciudad' => 'Colima, Colima',
    'direccion' => 'Calle 16 de septiembre'
));

$matricula = $_POST["matricula"];

if($matricula  == 'ABC0129' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula1);
}
if($matricula  == 'CDE1238' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula2);
}
if($matricula  == 'FGH3457' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula3);
}
if($matricula  == 'IJK5670' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula4);
}
if($matricula  == 'LMN9012' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula5);
}
if($matricula  == 'OPQ8123' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula6);
}
if($matricula  == 'RST7345' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula7);
}
if($matricula  == 'UVW0765' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula8);
}
if($matricula  == 'ZAB0123' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula9);
}
if($matricula  == 'BCD4567' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula10);
}
if($matricula  == 'DFG9013' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula11);
}
if($matricula  == 'HIJ3456' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula12);
}
if($matricula  == 'KLM7890' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula13);
}
if($matricula  == 'AJK0928' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula13);
}
if($matricula  == 'ABC0419' ){
    echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Los datos obtenidos de la matricula son:</font>';
    print '<pre>';
    print_r($matricula13);
}


        if($matricula  == 'si')
        {
            echo '<font size="4" face="Ms Sans Serif" color="#483D8B">
    Todos los datos de los registros de autos son:</font>';
            print '<pre>';
            print_r($arreglo = array_merge($matricula1,$matricula2,$matricula3,$matricula4,$matricula5,
            $matricula6,$matricula7,$matricula8,$matricula9,$matricula10,
            $matricula11,$matricula12,$matricula13,$matricula14,$matricula15));
        }

    ?> 

    </body>
</html>